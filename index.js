const Koa = require('koa2');
const koaBody = require('koa-body');
let bookModel = require('./models/bookModel')

const app = new Koa();
app.use(koaBody());

let books = [];

updateBooks();


app.use(function (ctx, next) {
    if (ctx.path !== '/add') {
        return next();
    }
    return parseCreatingBookFields(ctx)
        .then(bookModel.addBook)
        .then(addBookToCache)
        .then((newBook) => {
            ctx.body = {id: newBook.id};
            console.log("added book with id ", newBook.id)
        }).catch((err) => {
            console.log(err);
            ctx.throw(400, err);
        });
});


app.use(function (ctx, next) {
    if (ctx.path !== '/edit') {
        return next();
    }
    return parseUpdatingBookFields(ctx)
        .then(bookModel.editBook)
        .then(editInCache)
        .then((response) => {
            ctx.body = 'Fields changed!';
            console.log("All ok in update fields");
        }).catch((err) => {
            console.log(err);
            ctx.throw(400, err);
        });
});


app.use(function (ctx, next) {
    if (ctx.path !== '/get') {
        return next();
    }
    return parseGetBooksParams(ctx)
        .then(getBooks)
        .then((response) => {
            ctx.body = response;
            console.log("requested books...");
        }).catch((err) => {
            console.log(err);
            ctx.throw(400, err);
        })
});

function parseCreatingBookFields(ctx) {
    return new Promise((resolve, reject) => {
        const params = ctx.request.body;
        if (!params.title || !params.date || !params.author || !params.description || !params.image) {
            reject("Some of creating required fields are undefined");
            return;
        }
        resolve(params);
    })
}

function parseUpdatingBookFields(ctx) {
    return new Promise((resolve, reject) => {
        const params = ctx.request.body;
        if (typeof params.id !== 'undefined') {
            resolve(makeBook(params.id, params.title, params.date, params.author, params.description, params.image))
        } else {
            reject("Select id of updating book!")
        }
    })
}

function parseGetBooksParams(ctx) {
    return new Promise((resolve, reject) => {
        const params = ctx.request.body;
        if (typeof params.id !== 'undefined') {
            resolve([params.id]);
        } else {
            resolve(params);
        }
    })
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function updateBooks() {
    console.log('server init started');
    bookModel.getAll().then(res => {
        let rawBooksData = res;
        books = rawBooksData.map(function (rawBook) {
            return makeBook(rawBook.id, rawBook.title, rawBook.date, rawBook.author, rawBook.description, rawBook.image);
        });
        console.log('server init finished');
    })
}


function makeBook(id, title, date, author, description, image) {
    return {id: id, title: title, date: date, author: author, description: description, image: image};
}

function addBookToCache(newBook) {
    return new Promise((resolve, reject) => {
        books.push(newBook);
        resolve(newBook);
    })
}

function editInCache(changedBook) {
    return new Promise((resolve, reject) => {
        const changedBookFields = Object.keys(changedBook);
        changedBookFields.forEach(field => {
            if (field != 'id' && changedBook[field]) {
                books[changedBook.id - 1][field] = changedBook[field];
            }
        })
        resolve(changedBook);
    })
}

function getBooks(args) {
    return new Promise((resolve, reject) => {
            const loadedBooksAfterParsingGetArgs = function () {
                switch (args.length) {
                    case 1:
                        const inquiredBook = getBookById(args[0]);
                        if (inquiredBook) {
                            return [inquiredBook];
                        }
                        else {
                            reject("Book with id " + args[0].toString() + ' not found');
                            return [];
                        }
                        break;
                    default: {
                        const canBeSortedIfFieldDefined = function () {
                            const fieldName = args[3];
                            if (fieldName) {
                                return books[0][fieldName];
                            } else {
                                return true;
                            }
                        }()
                        if (canBeSortedIfFieldDefined) {
                            return getBooksPart(args[0], args[1], args[2], args[3], args[4]);
                        } else {
                            reject("Cant sort by undefined book field: " + args[3].toString());
                            return [];
                        }
                        break;
                    }
                }
            }
            ();
            resolve(loadedBooksAfterParsingGetArgs);
        }
    )
}

function comparatorForField(field) {
    switch (field) {
        case 'author':
        case 'title':
        case 'image':
        case 'description':
            return stringComparator(field);
            break;
        default:
            return commonComparator(field);
            break;
    }
}

function stringComparator(field) {
    return function (a, b) {
        return a[field].localeCompare(b[field]);
    }
}

function commonComparator(field) {
    return function (a, b) {
        if (a[field] > b[field]) {
            return 1;
        }
        if (a[field] < b[field]) {
            return -1;
        }
        return 0;
    }
}

function getBookById(id) {
    const inquiredIndex = id - 1;
    return books[inquiredIndex];
}

function getBooksPart(startIndex, count, offset, orderByField, descendingOrder) {
    let sortedBooks = function (books, sortField, descSorting) {
        if (sortField) {
            if (descSorting) {
                return books.slice().sort(comparatorForField(sortField)).reverse();
            } else {
                return books.slice().sort(comparatorForField(sortField));
            }
        } else {
            return books.slice();
        }
    }(books, orderByField, descendingOrder);
    const leftIndex = function (startIndex, offset) {
        if (offset) {
            return startIndex + offset;
        } else {
            return startIndex;
        }
    }(startIndex, offset);
    const rightIndex = leftIndex + count;
    return sortedBooks.slice(leftIndex, rightIndex);
}


app.listen(3000);