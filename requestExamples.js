let request = require('request');


sendJson([0, 10], 'get');               //get first 10 books

//sendJson({'id':12},'get');            //get single book by id
//sendJson([0,10,2],'get');             //get first 10 books with 2 offset
//sendJson([0,10,0,'author'],'get');    //get first 10 books with sorted by author field
//sendJson([0,10,0,'id',1],'get');      //get last 10 books

/*sendJson({
    title: 'World War Z',
    date: new Date(),
    author: 'Elite author',
    description: 'good book so',
    image: 'previewURL'
}, 'add');                              //create new book
*/

/*
sendJson({
    id:100059,
    author: 'Fake author',
    description: 'no so good book',
    image2: 'WrongFieldWillBeIgnored'
}, 'edit');                             //update not undefined existing in book fields by id, other are ignored
*/


function sendJson(json, postfix) {
    request.post(
        {
            url: 'http://localhost:3000/' + postfix,
            body: json,
            json: true
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body)
            } else {
                console.log(error);
                console.log(body);
            }
        }
    )
};