index.js - http-server

requestExamples.js - примеры запросов на сервер

Сервер ожидает запросы


'http://.../add' с объектом в качестве body

{title : String,
date : Date(),
author : String,
description : String,
image : String}


'http://.../edit' с объектом в качестве body

{id: Int,
date : Date(),
author : String,
description : String,
image : String}

Изменены будут поля у книги заданным id. 
Если какие-то поля опущены - останутся прежними.


'http://.../get' с объектом\массивом в качестве body

{id: int}
для получения книги по id

[startIndex, count, offset, orderByField, descOrder]
Для получения куска данных
startIndex, count, offset - Int
orderByField - String
descOrder - bool

