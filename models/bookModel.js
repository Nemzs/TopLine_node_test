const mysql = require('promise-mysql');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'ophaesun',
    database: 'topline',
    connectionLimit: 10
});

module.exports = {
    addBook: function (book) {
        const createInsertingSql = function () {
            return 'INSERT INTO books(title,date,author,description,image)' +
                'VALUES ("' + book.title + '", "' + formatDateToMysql(book.date) + '", "' + book.author + '",' +
                ' "' + book.description + '", "' + book.image + '");';
        }();

        return pool.query(createInsertingSql).then(queryResponse => {
            book.id = queryResponse.insertId;
            return book;
        }).catch(err => {
            console.log(err)
        });
    },
    editBook: function (book) {
        book.date = formatDateToMysql(book.date);

        const createUpdatingSql = function () {
            const updatingFields = Object.keys(book).filter((field) => {
                const ignoringIdField = field != 'id';
                const notUndefined = book[field];
                return ignoringIdField && notUndefined;
            })
                .map(function (fieldPair) {
                    return fieldPair + '= "' + book[fieldPair] + '"';
                }).join(', ');
            return 'update books set ' + updatingFields + ' where id =' + book.id;
        }();

        return pool.query(createUpdatingSql).then(queryResponse => {
            return book;
        }).catch(err => {
            console.log(err)
        });
    },
    getAll: function () {
        return pool.query("select * from books ");
    }
};

function formatDateToMysql(date) {
    if (date) {
        const parseDate = new Date(date);
        return parseDate.toISOString().slice(0, 19).replace('T', ' ');
    } else {
        return undefined;
    }
}