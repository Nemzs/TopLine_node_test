CREATE TABLE books (
    id INT NOT NULL AUTO_INCREMENT,
    title varchar(50) NOT NULL,
    date DATE,
    author varchar(50),
    description varchar(255),
    image varchar(255),
    PRIMARY KEY (id)
);